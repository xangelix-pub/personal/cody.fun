---
# Display name
title: Cody Neiman

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Security & Solutions Engineer

# Organizations/Affiliations to show in About widget
organizations:
  - name: Yale University
    url: https://yale.edu/

# Short bio (displayed in user profile at end of posts)
bio: Chasing inspirations...

# Interests to show in About widget
interests:
  - Security Research & Consulting
  - STEAM Outreach & Education
  - Music Composition
  - Entrepreneurship

# Education to show in About widget
education:
  courses:
    - course: Computer Science and Economics, Bachelor of Science
      institution: Yale College
      year: 2024
    - course: IB Diploma
      institution: Cocoa Beach Junior/Senior High School
      year: 2020

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:cody-neiman@cody.to'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: in
- icon: gitlab
  icon_pack: fab
  link: gitlab
- icon: github
  icon_pack: fab
  link: github
- icon: comments
  icon_pack: fas
  link: matrix
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
- icon: cv
  icon_pack: ai
  link: resume

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true
---

    Information Security marks one of the greatest challenges for the ever-developing demands of modern business. Whether an established enterprise with simultaneous, critical availability demands across composable infrastructure, or a start-up with the pricing requirements to scale from thousands of users to millions in seconds, great security is of complete obligation.

    As business Information Technology needs grow, so do cybersecurity threats. Your focus, management, and engineering must develop alongside these to protect the confidentiality, integrity, and availability of your information systems—amidst legal requirements, preparing and responding to disaster, and constant technology deprecation.

    In my work, I strive to prepare your company against evolving security compliance standards, establishing business frameworks for continuous integration of both physical and cyber security in your solutions, and preparing for emergency response.

{{< icon name="download" pack="fas" >}} Download my {{< staticref "/resume" "newtab" >}}résumé{{< /staticref >}}.
