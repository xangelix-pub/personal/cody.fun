---
# An instance of the Accomplishments widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: accomplishments

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 50

# Note: `&shy;` is used to add a 'soft' hyphen in a long heading.
title: 'Accomplish&shy;ments'
subtitle:

# Date format
#   Refer to https://wowchemy.com/docs/customization/#date-format
date_format: Jan 2006

# Accomplishments.
#   Add/remove as many `item` blocks below as you like.
#   `title`, `organization`, and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
item:
  - certificate_url: 'https://www.credly.com/badges/6ff60493-ffaf-487b-ae7d-fc1e05b8a6bf'
    certificate_file: '/assets/files/CodyNeiman-CompTIA-CASP+.pdf'
    date_end: '2025-09-21'
    date_start: '2022-09-21'
    description: '<img src="/assets/img/cert/casp+.svg" alt="CompTIA CASP+" title="CompTIA CASP+" style="height: 10em; width: 10em;" /><br />'
    organization: 'CompTIA'
    organization_url: 'https://www.comptia.org'
    title: 'CompTIA CASP+ ce Certification'
    url: 'https://www.comptia.org/certifications/comptia-advanced-security-practitioner'
  - certificate_url: 'https://lpi.org/v/LPI000495231/gqxg75a3xr'
    certificate_file: '/assets/files/CodyNeiman-LPI-Linux-Essentials.pdf'
    date_end: ''
    date_start: '2021-07-28'
    description: '<img src="/assets/img/cert/le.webp" alt="LPI Linux Essentials" title="LPI Linux Essentials" style="height: 10em; width: 10em;" /><br />'
    organization: 'Linux Professional Institute'
    organization_url: 'https://www.lpi.org/'
    title: 'LPI Linux Essentials'
    url: 'https://www.lpi.org/our-certifications/linux-essentials-overview'
  - certificate_url: 'https://www.credly.com/badges/fe9f5a6f-37c2-43b3-987d-5bed22bec81c/'
    certificate_file: '/assets/files/CodyNeiman-AWS-CCP.pdf'
    date_end: '2024-07-09'
    date_start: '2021-07-09'
    description: '<img src="/assets/img/cert/aws-ccp.webp" alt="AWS Certified Cloud Practitioner" title="AWS Certified Cloud Practitioner" style="height: 10em; width: 10em;" /><br />'
    organization: 'Amazon Web Services'
    organization_url: 'https://aws.amazon.com/'
    title: 'AWS Certified Cloud Practitioner'
    url: 'https://aws.amazon.com/certification/certified-cloud-practitioner/'
  - certificate_url: 'https://nesa.org/about-nesa/contact-nesa/'
    certificate_file: '/assets/files/CodyNeiman-BSA-EagleScout.pdf'
    date_end: ''
    date_start: '2020-01-01'
    description: '<img src=/assets/img/cert/eagle-scout.svg alt="Eagle Scout" title="Eagle Scout" style="height: 10em; width: 10em;" /><br />'
    organization: 'Boy Scouts of America'
    organization_url: 'https://scouting.org'
    title: 'Eagle Scout'
    url: 'https://nesa.org/'
  - certificate_url: 'https://www.credly.com/badges/ce148c60-58fc-4f21-83a6-ecf06a774611/'
    certificate_file: '/assets/files/CodyNeiman-CompTIA-CySA+.pdf'
    date_end: '2025-09-21'
    date_start: '2019-09-24'
    description: '<img src=/assets/img/cert/cysa+.svg alt="CompTIA CySA+" title="CompTIA CySA+" style="height: 10em; width: 10em;" /><br />'
    organization: 'CompTIA'
    organization_url: 'https://www.comptia.org'
    title: 'CompTIA CySA+ ce Certification'
    url: 'https://certification.comptia.org/certifications/cybersecurity-analyst'
  - certificate_url: 'https://www.credly.com/badges/e67bad34-a38f-4e86-b476-6c04cda8fa15'
    certificate_file: '/assets/files/CodyNeiman-CompTIA-PenTest+.pdf'
    date_end: '2025-09-21'
    date_start: '2019-08-03'
    description: '<img src=/assets/img/cert/pentest+.svg alt="CompTIA PenTest+" title="CompTIA PenTest+" style="height: 10em; width: 10em;" /><br />'
    organization: 'CompTIA'
    organization_url: 'https://www.comptia.org'
    title: 'CompTIA PenTest+ ce Certification'
    url: 'https://certification.comptia.org/certifications/pentest'
  - certificate_url: 'https://www.credly.com/badges/0b61d85a-9ca0-46ce-b9f2-f2ee4cd4aa4b'
    certificate_file: '/assets/files/CodyNeiman-CompTIA-Security+.pdf'
    date_end: '2025-09-21'
    date_start: '2019-05-23'
    description: '<img src=/assets/img/cert/security+.svg alt="CompTIA Security+" title="CompTIA Security+" style="height: 10em; width: 10em;"/><br />'
    organization: 'CompTIA'
    organization_url: 'https://www.comptia.org'
    title: 'CompTIA Security+ ce Certification'
    url: 'https://certification.comptia.org/certifications/security'
  - certificate_url: 'https://www.credly.com/badges/c0ca7282-c3a2-4491-bd6d-03f854c40aa1'
    certificate_file: '/assets/files/CodyNeiman-CompTIA-Network+.pdf'
    date_end: '2025-09-21'
    date_start: '2019-04-29'
    description: '<img src=/assets/img/cert/network+.svg alt="CompTIA Network+" title="CompTIA Network+" style="height: 10em; width: 10em;" /><br />'
    organization: 'CompTIA'
    organization_url: 'https://www.comptia.org'
    title: 'CompTIA Network+ ce Certification'
    url: 'https://certification.comptia.org/certifications/network'
  - certificate_url: 'https://www.credly.com/badges/67b9e588-82e7-4d74-a5f5-50ab767688f3/'
    certificate_file: '/assets/files/CodyNeiman-Microsoft-MTA-Networking-Fundamentals.pdf'
    date_end: '2025-09-21'
    date_start: '2018-08-27'
    description: '<img src=/assets/img/cert/mta-networking-fundamentals.webp alt="MTA: Networking Fundamentals" title="MTA: Networking Fundamentals" style="height: 10em; width: 10em;" /><br />'
    organization: 'Microsoft'
    organization_url: 'https://microsoft.com'
    title: 'MTA: Networking Fundamentals'
    url: 'https://www.microsoft.com/en-us/learning/exam-98-366.aspx'
  - certificate_url: 'https://www.credly.com/badges/c6599bc4-6fdd-489e-9d00-dffaf58ee773'
    certificate_file: '/assets/files/CodyNeiman-CompTIA-A+.pdf'
    date_end: '2025-09-21'
    date_start: '2018-04-03'
    description: '<img src=/assets/img/cert/a+.svg alt="CompTIA A+" title="CompTIA A+" style="height: 10em; width: 10em;" /><br />'
    organization: 'CompTIA'
    organization_url: 'https://www.comptia.org'
    title: 'CompTIA A+ ce Certification'
    url: 'https://certification.comptia.org/certifications/a'
  - certificate_url_word: 'https://www.credly.com/badges/3da65dc0-a10c-4771-8a87-4931ead36ebf'
    certificate_url_powerpoint: 'https://www.credly.com/badges/c920ca7a-675f-451c-b1b5-698f783cb790'
    certificate_url_excel: 'https://www.credly.com/badges/fc711472-4189-4bab-8ac7-f9916124d3c7'
    certificate_file_word: '/assets/files/CodyNeiman-Microsoft-MOS-Office-Word-2013.pdf'
    certificate_file_powerpoint: '/assets/files/CodyNeiman-Microsoft-MOS-Office-PowerPoint-2013.pdf'
    certificate_file_excel: '/assets/files/CodyNeiman-Microsoft-MOS-Office-Excel-2013.pdf'
    date_end: ''
    date_start: '2016-01-01'
    description: '<img src=/assets/img/cert/mos.webp alt="Microsoft Office 2013 Specialist" title="Microsoft Office 2013 Specialist" style="height: 8em; width: 12.416em;" /><br />'
    organization: 'Microsoft'
    organization_url: 'https://microsoft.com'
    title: 'Microsoft Office 2013 Specialist'
    url: 'https://www.microsoft.com/en-us/learning/microsoft-office-specialist-certification-2013.aspx'

design:
  columns: '2' 
---
