---
# An instance of the Experience widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: experience

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Experience
subtitle:

# Date format for experience
#   Refer to https://wowchemy.com/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` items below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
experience:
  - title: Incident Response Intern
    company: Unit 42 (Crypsis Group) - Palo Alto Networks
    company_url: 'https://www.paloaltonetworks.com/unit42'
    company_logo: unit42
    location: Reston, Virginia, United States
    date_start: '2022-05-23'
    date_end: '2023-05-31'
    description: |2-
      * Performed digital forensics and incident response procedures on cases involving ransomware, phishing, insider threat, hypervisor compromise, and oauth-based attacks, led client-facing discovery meetings, and managed Endpoint Detection and Response installations for Live Response cases
      * Created Python APIs for automating and programmatically triggering case activities, newly making any-scale forensic workflow processing possible

  - title: Security Consultant Intern
    company: Altooro
    company_url: 'https://altooro.com/'
    company_logo: altooro
    location: Tel Aviv, Israel
    date_start: '2021-02-01'
    date_end: '2021-05-01'
    description: |2-
      * Executed full-stack, cloud architecture security study, penetration test, and POA&M reporting based on the FedRAMP framework.
      * Prepared technology and business infrastructure for automated (DevOps-based) security compliance analysis.
        
  - title: Information Security Intern
    company: National Aeronautics and Space Administration
    company_url: 'https://nasa.gov/'
    company_logo: nasa
    location: Kennedy Space Center, Florida, United States
    date_start: '2020-06-01'
    date_end: '2020-08-07'
    description: |2-
      * Co-authored formal security investigation of the EGS Launch Control Systems—identifying significant, previously unknown deficiencies.
      * Constructed automated, network-scale dependency-tree databasing system from the ground up for the large-scale tracking of project security.
        
  - title: Information Security Intern
    company: National Aeronautics and Space Administration
    company_url: 'https://nasa.gov/'
    company_logo: nasa
    location: Kennedy Space Center, Florida, United States
    date_start: '2019-06-03'
    date_end: '2019-08-09'
    description: |2-
      * Led programmatic security compliance analysis and systems penetration testing in the Launch Control Center (LCC).
      *  Invented new programmatic approach to cross-application Key Risk Indicator (KRI) testing and reporting in production systems, resulting in dramatic process improvements to platform integration and proof-based security procedures.

  - title: Data Science Intern
    company: Clean Footprint
    company_url: 'http://www.clean-footprint.com/'
    company_logo: clean_footprint
    location: Cape Canaveral, Florida, United States
    date_start: '2017-11-01'
    date_end: '2018-01-01'
    description: |2-
      * Developed user-friendly Optical Character Recognition (OCR) scanning application in C# to automatically update information in a relational database from paper documents, accelerating productivity of a previously labor-intensive task.

  - title: Infrastructure Planning and Laborer
    company: Sunrise Bath & Tile
    company_url: 'https://www.sunrisebathandtile.com/'
    company_logo: sunrise_bath_and_tile
    location: Cocoa Beach, Florida, United States
    date_start: '2017-06-01'
    date_end: '2017-08-01'
    description: |2-
      * Collaborated with mentors and employees to plan architectural layouts and install bath and tile related utilities and structures.

design:
  columns: '2'
---
